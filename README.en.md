# musi Tokusu Monokatali

toki pona translation patch for Cave Story

[README.md (toki pona)](README.md)

README.jp.md (日本語) lon ala 

[![Latest Release](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/badges/release.svg)](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/releases)

Base translation files only.  For specific game engines, please see [nasin](nasin)

# Building

Clone the repo

```
git clone https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali.git
cd musi-Tokusu-Monokatali
```

## Setup game engine(s)

Use one of the following to install and setup a game engine

`git submodule init nasin/nxengine` # For Retroarch

`git submodule init nasin/nxengine-evo` # Recommended

`git submodule init nasin/doukusu-rs` # Best Graphics

`git submodule init` # ALL

## make

Build translations

`make LANG=` All writing systems

`make LANG=en` Basic Latin only

`make LANG=jp` Hiragana only (UTF8 and SJIS)

`make LANG=tok` sitelen pona only

## install and run

See engine specific instructions:

- [nxengine](https://gitlab.com/lupa-Tokusu-Monokatali/nxengine)
- [nxengine-evo](https://gitlab.com/lupa-Tokusu-Monokatali/nxengine-evo)
- [doukusu-rs](https://gitlab.com/lupa-Tokusu-Monokatali/doukusu-rs)

# TO-DO
many things

- [ ] */README.jp.md
- [ ] nasin
  - [x] Freeware on Windows (tested on XP)
  - [ ] nxengine
  	- [x] Retroarch
    - [x] Android (kepeken ilo Retroarch)
    - [x] Linux 
    - [ ] ilo Macintosh
  - [ ] nxengine-evo
    - [ ] Macintosh
	- [x] Linux
	- [x] Switch
	- [x] Windows
  - [ ] doukutsu-rs
    - [x] Android
	- [x] Linux 
    - [ ] Macintosh
	- [ ] Windows
- [x] sitelen toki
  - [x] sitelen Lasina
  - [x] sitelen Ilakana
  - [x] sitelen pona
- [ ] toki ante
  - [x] Doukutsu.exe
  - [ ] mrmap.bin
  - [ ] data
    - [x] data/ArmsItem.tsc
    - [ ] data/Caret.pbm
    - [x] data/Credit.tsc
    - [x] data/Head.tsc
    - [x] data/StageSelect.tsc
    - [ ] data/TextBox.pbm
    - [x] data/Title.pbm
  - [x] data/Npc
    - [x] data/Npc/NpcSym.bmp
  - [x] data/Stage/*
  - [x] data/stage.tbl (doukutsu-rs)
  - [x] data/system.json (NXEngine-evo)
