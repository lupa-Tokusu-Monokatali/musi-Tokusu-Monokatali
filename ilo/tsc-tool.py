#!/usr/bin/python3
#
# Copyright (C) kulupu pakola li kili nasa
#
# This work is free.  You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To But It's Not My Fault Public
# License, Version 1, as published by Ben McGinnes.  See the
# COPYING.WTFNMFPL text file for more details.

import os
import sys
import argparse
from io import BytesIO
from io import BufferedRWPair

def encode(inFile, outFile, encoding=None):
    '''Encode or decode a tsc file'''
    
    inBuffer = inFile
    
    if(not inBuffer.seekable()):
        # Copy to a seekable BytesIO
        inBuffer = BytesIO(b'')
        while(b := inFile.read(1)):
            inBuffer.write(b)
    else:
        inBuffer.seek(0,2)
        
    e = encoding
    # Guess encoding if not specified
    # If first byte is a CR, LF or #, asume we are encoding a plain txt file
    # Otherwise assume we decoding an encoded tsc file
    if(not encoding):
        inBuffer.seek(0)
        e = -1 if b'\x0a\x0d\x23'.find(inBuffer.read(1)) else 1

    inBuffer.seek(0,2)  
    m = int(inBuffer.tell()/2)
    inBuffer.seek(m)
    k = (1 if e > 0 else -1 if e < 0 else 0) * int.from_bytes(inBuffer.read(1), byteorder='big', signed=False)
    
    # Apply the encoding and write
    inBuffer.seek(0)
    rwPair = BufferedRWPair(inBuffer, outFile)
    
    for i in range(0, m):
        b = rwPair.read(1)
        rwPair.write( ( 0xFF & ( int.from_bytes(b, byteorder='big') + k) ).to_bytes(1, byteorder='big') )

    b = rwPair.read(1)
    rwPair.write(b)
    
    while(b := rwPair.read(1)):
        rwPair.write( ( 0xFF & ( int.from_bytes(b, byteorder='big') + k) ).to_bytes(1, byteorder='big') )
    
    return

def main(argv = None):
    parser = argparse.ArgumentParser(
        prog = 'tsc-tool.py',
        description = '''Encode or Decode .tsc scripts
        If --decode and --encode options are not specified,
        try to automatically detect the desired action by
        file extension if provided, otherwise by file data.
        If invalid options or file extensions are used,
        cowardly print this message and exit.'''
    )
    
    parser.add_argument('-d', '--decode', dest='DECODE', action='store_true',
        help='Decode a file')
    parser.add_argument('-e', '--encode', dest='ENCODE', action='store_true',
        help='Encode a file')
    parser.add_argument('IN_FILE', nargs='?', default='-')
    parser.add_argument('OUT_FILE', nargs='?', default='-')
    
    args = parser.parse_args(sys.argv[1:] if argv is None else argv)

    # Exit if invalid args
    if( (args.ENCODE and args.DECODE ) or
        ( args.IN_FILE.endswith('.tsc') and args.OUT_FILE.endswith('.tsc') ) or
        ( args.IN_FILE.endswith('.txt') and args.OUT_FILE.endswith('.txt') ) ):
        args.print_help()
        return
    
    # open files
    if(args.IN_FILE == '-'):
        inFile = sys.stdin.buffer
    else:
        inFile = open(args.IN_FILE, 'rb')
        
    if(args.OUT_FILE == '-'):
        outFile = sys.stdout.buffer
    else:
        outFile = open(args.OUT_FILE, 'wb')
        
    # Encode a file
    if( args.ENCODE or
        ( args.IN_FILE.endswith('.txt') and not args.OUT_FILE.endswith('.txt') ) or
        ( args.OUT_FILE.endswith('.tsc') and not args.OUT_FILE.endswith('.tsc') ) ):
        encode(inFile, outFile, 1)
        inFile.close()
        outFile.close()
        return

    # Decode a file
    if( args.DECODE or
        ( args.IN_FILE.endswith('.tsc') and not args.OUT_FILE.endswith('.tsc') ) or
        ( args.OUT_FILE.endswith('.txt') and not args.OUT_FILE.endswith('.txt') ) ):
        encode(inFile, outFile, -1)
        inFile.close()
        outFile.close()
        return

    # Guess from file contents
    encode(inFile, outFile, 0)
    inFile.close()
    outFile.close()
    return
    
if __name__ == "__main__":
    main()
