#!/usr/bin/python3
#
# Copyright (C) kulupu pakola li kili nasa
#
# This work is free.  You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To But It's Not My Fault Public
# License, Version 1, as published by Ben McGinnes.  See the
# COPYING.WTFNMFPL text file for more details.

import os
import sys
import argparse
from io import BytesIO
from io import BufferedRWPair


def verifyTSC(inFileName, outFileName, LENGTH_MAX=32, LENGTH_FACE=24):

        # open files
    if(inFileName == '-'):
        inFile = sys.stdin.buffer
    else:
        inFile = open(inFileName, 'r')
        
    if(outFileName == '-'):
        outFile = sys.stdout.buffer
    else:
        outFile = open(outFileName, 'w')

        
    lineNumber = 0
    lineLength = 0
    face = None
    for line in inFile:
        lineNumber = lineNumber + 1
        # Reset on new script
        if(line.startswith('#')):
            lineLength = 0
            face = None
        if(line.startswith('<')):
            cmd = line[1:4]
            args = (lambda x: x if (len(x[0]) > 0 ) else [] )(line[4:len(line)-1].split(':'))
            if(cmd == 'FAC'):
                if(args[0] == '0000'):
                    face = args[0]
                else:
                    face = None
        
            # if(cmd=='MNP' or cmd=='SNP' or cmd=='TRA' ):
            
            # elif( cmd=='ANP' or cmd=='CMP' or cmd=='CNP' or cmd=='INP' or cmd=='TAM' ):
            
            # elif( cmd=='AM+' or cmd=='AMJ' or cmd=='ECJ' or cmd=='FLJ' or cmd=='FO' or cmd=='FON' or cmd=='ITJ' or cmd=='MOV' or cmd=='NCJ' or cmd=='PS+' or cmd=='SKJ' or cmd=='SMP' or cmd=='UNJ' ):
            
            # elif( cmd=='AM-' or cmd=='BOA' or cmd=='BSL' or cmd=='CMU' or cmd=='DNA' or cmd=='DNP' or cmd=='EQ+' or cmd=='EQ-' or cmd=='EVE' or cmd=='FAC' or cmd=='FAI' or cmd=='FAO' or cmd=='FL+' or cmd=='FL-' or cmd=='FOM' or cmd=='GIT' or cmd=='IT+' or cmd=='IT-' or cmd=='LI+' or cmd=='ML+' or cmd=='MP+' or cmd=='MPJ' or cmd=='MY' or cmd=='MYD' or cmd=='NUM' or cmd=='QUA' or cmd=='SIL' or cmd=='SK+' or cmd=='SK-' or cmd=='SOU' or cmd=='SSS' or cmd=='UNI' or cmd=='WAI' or cmd=='XX1' or cmd=='YNJ' ):
            
            # elif( cmd=='AE+' or cmd=='CAT' or cmd=='CIL' or cmd=='CLO' or cmd=='CLR' or cmd=='CPS' or cmd=='CRE' or cmd=='CSS' or cmd=='END' or cmd=='ESC' or cmd=='FLA' or cmd=='FMU' or cmd=='FRE' or cmd=='HMC' or cmd=='INI' or cmd=='KEY' or cmd=='LDP' or cmd=='MLP' or cmd=='MM0' or cmd=='MNA' or cmd=='MS2' or cmd=='MS3' or cmd=='MSG' or cmd=='NOD' or cmd=='PRI' or cmd=='RMU' or cmd=='SAT' or cmd=='SLP' or cmd=='SMC' or cmd=='SPS' or cmd=='STC' or cmd=='SVP' or cmd=='TUR' or cmd=='WAS' or cmd=='ZAM' ):
                
            # else:

            # print( str(lineNumber) + ':\t<' + cmd + ' ' + str(args))
        if(not line.startswith('<')):
            if(face is not None and (len(line)-1 > LENGTH_FACE)):
                print(f'{inFileName}:{str(lineNumber)} ({len(line)-1})\t<FAC{face}{line[:len(line)-1]}')
            if(face is None and (len(line)-1 > LENGTH_MAX)):
                print(f'{inFileName}:{str(lineNumber)} ({len(line)-1})\t{line[:len(line)-1]}')
    
    inFile.close()
    outFile.close()
    return

def main(argv = None):
    parser = argparse.ArgumentParser(
        prog = 'tsc-verify.py',
        description = '''Verify line-lengths of .tsc files.
        Requires pretty-tsc format.
        Prints lines that may be too long.
        '''
    )
        
    parser.add_argument('-l', '--length-max', dest='LENGTH_MAX', type=int, default=32,
                        help='Specify max length. Default 32')
    parser.add_argument('-f', '--face-max', dest='LENGTH_FACE', type=int, default=24,
                        help='Specify max length when facepic is displayed. Default 24')
    parser.add_argument('IN_FILE', nargs='?', default='-')
    parser.add_argument('OUT_FILE', nargs='?', default='-')

    args = parser.parse_args(sys.argv[1:] if argv is None else argv)



    verifyTSC(args.IN_FILE, args.OUT_FILE, LENGTH_MAX=args.LENGTH_MAX, LENGTH_FACE=args.LENGTH_FACE)

    return

if __name__ == "__main__":
    main()
