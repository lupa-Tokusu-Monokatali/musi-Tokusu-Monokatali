#!/usr/bin/python3
#
# Copyright (C) kulupu pakola li kili nasa
#
# This work is free.  You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To But It's Not My Fault Public
# License, Version 1, as published by Ben McGinnes.  See the
# COPYING.WTFNMFPL text file for more details.

import os
import sys
import argparse
from io import BytesIO
from io import BufferedRWPair

import json

def readDoukutsuExe(inFile, encoding='sjis', offset=0x000937B0):

    if(inFile.seekable()):
        inFile.read(offset)

    # Read 95 records, 200 bytes each
    stageData = []
    for i in range(0, 95):
        stageData.append( [ '', '', b'', '', '', '' , b'', '' ] )
        stageData[-1][0] = inFile.read(32).decode(encoding)
        stageData[-1][1] = inFile.read(32).decode(encoding)
        stageData[-1][2] = inFile.read(4)
        stageData[-1][3] = inFile.read(32).decode(encoding)
        stageData[-1][4] = inFile.read(32).decode(encoding)
        stageData[-1][5] = inFile.read(32).decode(encoding)
        stageData[-1][6] = inFile.read(1)
        stageData[-1][7] = inFile.read(35).decode(encoding)
    
    return stageData

def writeDoukutsuExe(outFile, stageData, encoding='sjis', offset=0x000937B0):
    
    if(outFile.seekable()):
        outFile.read(offset)
    
    # Pad each field with null bytes and write
    for fields in stageData:        
        fields[0] = None if fields[0] is None else fields[0].encode(encoding)[0:32].ljust(32,b'\x00')
        fields[1] = None if fields[1] is None else fields[1].encode(encoding)[0:32].ljust(32,b'\x00')
        fields[2] = None if fields[2] is None else fields[2].rjust(4,b'\x00')
        fields[3] = None if fields[3] is None else fields[3].encode(encoding)[0:32].ljust(32,b'\x00')
        fields[4] = None if fields[4] is None else fields[4].encode(encoding)[0:32].ljust(32,b'\x00')
        fields[5] = None if fields[5] is None else fields[5].encode(encoding)[0:32].ljust(32,b'\x00')
        fields[6] = None if fields[6] is None else fields[6].rjust(1,b'\x00')
        fields[7] = None if fields[7] is None else fields[7].encode(encoding)[0:35].ljust(35,b'\x00')

        if(fields[0] is None):
            if(outFile.seekable()):
                outFile.read(32)
        else:
            outFile.write(fields[0])

        if(fields[1] is None):
            if(outFile.seekable()):
                outFile.read(32) 
        else:
            outFile.write(fields[1])

        if(fields[2] is None):
            if(outFile.seekable()):
                outFile.read(4)
        else:
            outFile.write(fields[2])

        if(fields[3] is None):
            if(outFile.seekable()):
                outFile.read(32)
        else:
            outFile.write(fields[3])

        if(fields[4] is None):
            if(outFile.seekable()):
                outFile.read(32)
        else:
            outFile.write(fields[4])

        if(fields[5] is None):
            if(outFile.seekable()):
                outFile.read(32)
        else:
            outFile.write(fields[5])

        if(fields[6] is None):
            if(outFile.seekable()):
                outFile.read(1)
        else:
            outFile.write(fields[6])

        if(fields[7] is None):
            if(outFile.seekable()):
                outFile.read(35)
        else:
            outFile.write(fields[7])
        
    return



def readSystemJSON(inFile):
    # Encoding should be UTF-8
    jsonData = json.load(inFile)

    stageData = []
    for i in range(0, 95):
        stageData.append( [ None, None, None, None, None, None, None, None ] )
        
    stageData[ 0][7] = jsonData["Null"]                     # Null...............................
    stageData[ 1][7] = jsonData["Arthur's House"]           # Arthur's House.....................
    stageData[ 2][7] = jsonData["Egg Corridor"]             # Egg Corridor.......................
    stageData[ 3][7] = jsonData["Egg No. 00"]               # Egg No. 00.........................
    stageData[ 4][7] = jsonData["Egg No. 06"]               # Egg No. 06.........................
    stageData[ 5][7] = jsonData["Egg Observation Room"]     # Egg Observation Room...............
    stageData[ 6][7] = jsonData["Grasstown"]                # Grasstown..........................
    stageData[ 7][7] = jsonData["Santa's House"]            # Santa's House......................
    stageData[ 8][7] = jsonData["Chaco's House"]            # Chaco's House......................
    stageData[ 9][7] = jsonData["Labyrinth I"]              # Labyrinth I........................
    stageData[10][7] = jsonData["Sand Zone"]                # Sand Zone..........................
    stageData[11][7] = jsonData["Mimiga Village"]           # Mimiga Village.....................
    stageData[12][7] = jsonData["First Cave"]               # First Cave.........................
    stageData[13][7] = jsonData["Start Point"]              # Start Point........................
    stageData[14][7] = jsonData["Shack"]                    # Shack..............................
    stageData[15][7] = jsonData["Reservoir"]                # Reservoir..........................
    stageData[16][7] = jsonData["Graveyard"]                # Graveyard..........................
    stageData[17][7] = jsonData["Yamashita Farm"]           # Yamashita Farm.....................
    stageData[18][7] = jsonData["Shelter"]                  # Shelter............................
    stageData[19][7] = jsonData["Assembly Hall"]            # Assembly Hall......................
    stageData[20][7] = jsonData["Save Point"]               # Save Point.........................
    stageData[21][7] = jsonData["Side Room"]                # Side Room..........................
    stageData[22][7] = jsonData["Cthulhu's Abode"]          # Cthulhu's Abode....................
    stageData[23][7] = jsonData["Egg No. 01"]               # Egg No. 01.........................
    stageData[24][7] = jsonData["Arthur's House"]           # Arthur's House.....................
    stageData[25][7] = jsonData["Power Room"]               # Power Room.........................
    stageData[26][7] = jsonData["Save Point"]               # Save Point.........................
    stageData[27][7] = jsonData["Execution Chamber"]        # Execution Chamber..................
    stageData[28][7] = jsonData["Gum"]                      # Gum................................
    stageData[29][7] = jsonData["Sand Zone Residence"]      # Sand Zone Residence................
    stageData[30][7] = jsonData["Grasstown Hut"]            # Grasstown Hut......................
    stageData[31][7] = jsonData["Main Artery"]              # Main Artery........................
    stageData[32][7] = jsonData["Small Room"]               # Small Room.........................
    stageData[33][7] = jsonData["Jenka's House"]            # Jenka's House......................
    stageData[34][7] = jsonData["Deserted House"]           # Deserted House.....................
    stageData[35][7] = jsonData["Sand Zone Storehouse"]     # Sand Zone Storehouse...............
    stageData[36][7] = jsonData["Jenka's House"]            # Jenka's House......................
    stageData[37][7] = jsonData["Sand Zone"]                # Sand Zone..........................
    stageData[38][7] = jsonData["Labyrinth H"]              # Labyrinth H........................
    stageData[39][7] = jsonData["Labyrinth W"]              # Labyrinth W........................
    stageData[40][7] = jsonData["Camp"]                     # Camp...............................
    stageData[41][7] = jsonData["Clinic Ruins"]             # Clinic Ruins.......................
    stageData[42][7] = jsonData["Labyrinth Shop"]           # Labyrinth Shop.....................
    stageData[43][7] = jsonData["Labyrinth B"]              # Labyrinth B........................
    stageData[44][7] = jsonData["Boulder Chamber"]          # Boulder Chamber....................
    stageData[45][7] = jsonData["Labyrinth M"]              # Labyrinth M........................
    stageData[46][7] = jsonData["Dark Place"]               # Dark Place.........................
    stageData[47][7] = jsonData["Core"]                     # Core...............................
    stageData[48][7] = jsonData["Waterway"]                 # Waterway...........................
    stageData[49][7] = jsonData["Egg Corridor?"]            # Egg Corridor?......................
    stageData[50][7] = jsonData["Cthulhu's Abode?"]         # Cthulhu's Abode?...................
    stageData[51][7] = jsonData["Egg Observation Room?"]    # Egg Observation Room?..............
    stageData[52][7] = jsonData["Egg No. 00"]               # Egg No. 00.........................
    stageData[53][7] = jsonData["Outer Wall"]               # Outer Wall.........................
    stageData[54][7] = jsonData["Side Room"]                # Side Room..........................
    stageData[55][7] = jsonData["Storehouse"]               # Storehouse.........................
    stageData[56][7] = jsonData["Plantation"]               # Plantation.........................
    stageData[57][7] = jsonData["Jail No. 1"]               # Jail No. 1.........................
    stageData[58][7] = jsonData["Hideout"]                  # Hideout............................
    stageData[59][7] = jsonData["Rest Area"]                # Rest Area..........................
    stageData[60][7] = jsonData["Teleporter"]               # Teleporter.........................
    stageData[61][7] = jsonData["Jail No. 2"]               # Jail No. 2.........................
    stageData[62][7] = jsonData["Balcony"]                  # Balcony............................
    stageData[63][7] = jsonData["Final Cave"]               # Final Cave.........................
    stageData[64][7] = jsonData["Throne Room"]              # Throne Room........................
    stageData[65][7] = jsonData["The King's Table"]         # The King's Table...................
    stageData[66][7] = jsonData["Prefab Building"]          # Prefab Building....................  
    stageData[67][7] = jsonData["Last Cave (Hidden)"]       # Last Cave (Hidden)................. 
    stageData[68][7] = jsonData["Black Space"]              # Black Space........................
    stageData[69][7] = jsonData["Little House"]             # Little House.......................
    stageData[70][7] = jsonData["Balcony"]                  # Balcony............................
    stageData[71][7] = jsonData["Fall"]                     # Fall...............................
    stageData[72][7] = 'u'                                  # u..................................
    stageData[73][7] = jsonData["Waterway Cabin"]           # Waterway Cabin.....................
    stageData[74][7] = ''                                   # ...................................
    stageData[75][7] = ''                                   # ...................................
    stageData[76][7] = ''                                   # ...................................
    stageData[77][7] = ''                                   # ...................................
    stageData[78][7] = ''                                   # ...................................
    stageData[79][7] = jsonData["Prefab House"]             # Prefab House.......................
    stageData[80][7] = jsonData["Sacred Ground - B1"]       # Sacred Ground - B1.................
    stageData[81][7] = jsonData["Sacred Ground - B2"]       # Sacred Ground - B2.................
    stageData[82][7] = jsonData["Sacred Ground - B3"]       # Sacred Ground - B3.................
    stageData[83][7] = jsonData["Storage"]                  # Storage............................
    stageData[84][7] = jsonData["Passage?"]                 # Passage?...........................
    stageData[85][7] = jsonData["Passage?"]                 # Passage?...........................                                              
    stageData[86][7] = jsonData["Statue Chamber"]           # Statue Chamber.....................
    stageData[87][7] = jsonData["Seal Chamber"]             # Seal Chamber.......................
    stageData[88][7] = jsonData["Corridor"]                 # Corridor...........................
    stageData[89][7] = ''                                   # ...................................
    stageData[90][7] = jsonData["Hermit Gunsmith"]          # Hermit Gunsmith....................
    stageData[91][7] = ''                                   # ...................................
    stageData[92][7] = jsonData["Seal Chamber"]             # Seal Chamber.......................
    stageData[93][7] = ''                                   # ...................................
    stageData[94][7] = jsonData["Clock Room"]               # Clock Room.........................
        
    return stageData

def writeSystemJSON(outFile, stageData):
    # TODO
    return



def readMrmap(inFile, encoding='sjis'):
    # Skip header
    inFile.read(0x4)
    
    # Read 95 records, 116 bytes each
    stageData = []
    for i in range(0, 95):
        stageData.append( [ '', '', b'', '', '', '', b'', '' ] )
        stageData[-1][0] = inFile.read(16).decode(encoding)
        stageData[-1][1] = inFile.read(16).decode(encoding)
        stageData[-1][2] = inFile.read(1)
        stageData[-1][3] = inFile.read(16).decode(encoding)
        stageData[-1][4] = inFile.read(16).decode(encoding)
        stageData[-1][5] = inFile.read(16).decode(encoding)
        stageData[-1][6] = inFile.read(1)
        stageData[-1][7] = inFile.read(34).decode(encoding)

    return stageData

def writeMrmap(outFile, stageData, encoding='sjis'):
    # Write header
    outFile.write((95).to_bytes(4, 'little'))
    
    # Pad each field with null bytes and write
    for fields in stageData:
        fields[0] = None if fields[0] is None else fields[0].encode(encoding)[0:16].ljust(16,b'\x00')
        fields[1] = None if fields[1] is None else fields[1].encode(encoding)[0:16].ljust(16,b'\x00')
        fields[2] = None if fields[2] is None else fields[2][-1:].rjust(1,b'\x00')
        fields[3] = None if fields[3] is None else fields[3].encode(encoding)[0:16].ljust(16,b'\x00')
        fields[4] = None if fields[4] is None else fields[4].encode(encoding)[0:16].ljust(16,b'\x00')
        fields[5] = None if fields[5] is None else fields[5].encode(encoding)[0:16].ljust(16,b'\x00')
        fields[6] = None if fields[6] is None else fields[6][-1:].rjust(1,b'\x00')
        fields[7] = None if fields[7] is None else fields[7].encode(encoding)[0:34].ljust(34,b'\x00')

        if(fields[0] is None):
            if(outFile.seekable()):
                outFile.read(16)
        else:
            outFile.write(fields[0])

        if(fields[1] is None):
            if(outFile.seekable()):
                outFile.read(16)
        else:
            outFile.write(fields[1])

        if(fields[2] is None):
            if(outFile.seekable()):
                outFile.read(1)
        else:
            outFile.write(fields[2])

        if(fields[3] is None):
            if(outFile.seekable()):
                outFile.read(16)
        else:
            outFile.write(fields[3])

        if(fields[4] is None):
            if(outFile.seekable()):
                outFile.read(16)
        else:
            outFile.write(fields[4])

        if(fields[5] is None):
            if(outFile.seekable()):
                outFile.read(16)
        else:
            outFile.write(fields[5])

        if(fields[6] is None):
            if(outFile.seekable()):
                outFile.read(1)
        else:
            outFile.write(fields[6])

        if(fields[7] is None):
            if(outFile.seekable()):
                outFile.read(16)
        else:
            outFile.write(fields[7])
        
    return



def readStageTbl(inFile, JP=True, EN=True):
    # Read 95 records, 229 bytes each
    stageData = []
    for i in range(0, 95):
        stageData.append( [ '', '', b'', '', '', '' , b''] )
        stageData[-1][0] = inFile.read(32).decode("sjis")
        stageData[-1][1] = inFile.read(32).decode("sjis")
        stageData[-1][2] = inFile.read(4)
        stageData[-1][3] = inFile.read(32).decode("sjis")
        stageData[-1][4] = inFile.read(32).decode("sjis")
        stageData[-1][5] = inFile.read(32).decode("sjis")
        stageData[-1][6] = inFile.read(1)
        if(JP):
            stageData[-1].append(inFile.read(32).decode("sjis"))
        else:
            inFile.read(32)
            stageData[-1].append('')
        if(EN):
            stageData[-1].append(inFile.read(32).decode("utf8"))
        else:
            inFile.read(32)
            stageData[-1].append('')
            
    return stageData

def writeStageTbl(outFile, stageData, encoding='sjis', lang="en"):
    # Pad each field with null bytes and write
    for fields in stageData:
        fields[0] = None if fields[0] is None else fields[0].encode(encoding)[0:32].ljust(32,b'\x00')
        fields[1] = None if fields[1] is None else fields[1].encode(encoding)[0:32].ljust(32,b'\x00')
        fields[2] = None if fields[2] is None else fields[2].rjust(4,b'\x00')
        fields[3] = None if fields[3] is None else fields[3].encode(encoding)[0:32].ljust(32,b'\x00')
        fields[4] = None if fields[4] is None else fields[4].encode(encoding)[0:32].ljust(32,b'\x00')
        fields[5] = None if fields[5] is None else fields[5].encode(encoding)[0:32].ljust(32,b'\x00')
        fields[6] = None if fields[6] is None else fields[6].rjust(1,b'\x00')
        fields[7] = None if fields[7] is None else fields[7].encode(encoding)[0:32].ljust(32,b'\x00')
        if(len(fields) > 8):
            fields[8] = None if fields[8] is None else fields[8].encode(encoding)[0:32].ljust(32,b'\x00')

        if(fields[0] is None):
            if(outFile.seekable()):
                outFile.read(32)
        else:
            outFile.write(fields[0])

        if(fields[1] is None):
            if(outFile.seekable()):
                outFile.read(32)
        else:
            outFile.write(fields[1])

        if(fields[2] is None):
            if(outFile.seekable()):
                outFile.read(4)
        else:
            outFile.write(fields[2])

        if(fields[3] is None):
            if(outFile.seekable()):
                outFile.read(32)
        else:
            outFile.write(fields[3])

        if(fields[4] is None):
            if(outFile.seekable()):
                outFile.read(32)
        else:
            outFile.write(fields[4])

        if(fields[5] is None):
            if(outFile.seekable()):
                outFile.read(32)
        else:
            outFile.write(fields[5])

        if(fields[6] is None):
            if(outFile.seekable()):
                outFile.read(1)
        else:
            outFile.write(fields[6])

        if(len(fields) <= 8):

            outFile.write(32 * b'\x00')
            
            if(fields[7] is None):
                if(outFile.seekable()):
                    outFile.read(32)
            else:
                outFile.write(fields[7])
                
        else:
            if(fields[7] is None):
                if(outFile.seekable()):
                    outFile.read(32)
            else:
                outFile.write(fields[7])
            
            if(fields[8] is None):
                if(outFile.seekable()):
                    outFile.read(32)
            else:
                outFile.write(fields[7])
            
    return



def main(argv = None):
    parser = argparse.ArgumentParser(
        prog = 'cavestage.py',
        description = '''Copy or convert Cave Story stage data.
        
        Accepts "Doukutsu.exe" / ".csmap" / "csmap.bin", and "mrmap.bin" (CSE2), "system.json" (NXEngine-evo), and "stage.tbl" (CS+) files.
        If filename does not match one of the above, assume Doukutsu.exe / csmap binary format.

        NXEngine-evo "system.json" files only contain map names.  Additional sources are required.
        
        Column spec:
        1. Tileset name (PrtXXX.bmp/pxa)
        2. File name (XXX.pxm/pxe/tsc)
        3. Background scroll type (0x00000000 - 0x00000007)
        4. Background name
        5. NPC set 1 name
        6. NPC set 2 name
        7. Boss number (0x00 - 0x09)
        8. Map name (CS+: JP map name)
        9. English Map name (CS+ only)
            If reading from a non-CS+ format, read from the 8th column
        ''',
        formatter_class=argparse.RawTextHelpFormatter
    )
    
    parser.add_argument('SOURCE', nargs='?', default='-')
    parser.add_argument('DEST', nargs='?', default='-')

    parser.add_argument('-f', '--from-code', dest='DECODE', default="sjis",
                        help='Specify source encoding. (Default: sjis)')
    
    parser.add_argument('-t', '--to-code', dest='ENCODE', default="sjis",
                        help='Specify destination encoding. (Default: sjis)')

    args = parser.parse_args(sys.argv[1:] if argv is None else argv)
    
    stageData = []
    
    # open files
    if(args.SOURCE == '-'):
        # Assume raw binary if reading from stdin
        inFile = sys.stdin.buffer
        stageData = readDoukutsuExe(inFile, args.ENCODE, offset=0)
        
    else:
        inFile = open(args.SOURCE, 'rb')

        if(args.SOURCE.endswith("Doukutsu.exe")):  # Original
            stageData = readDoukutsuExe(inFile, args.DECODE)
        elif(args.SOURCE.endswith(".csmap")):
            stageData = readDoukutsuExe(inFile, args.DECODE, offset=0)
        elif(args.SOURCE.endswith("csmap.bin")):
            stageData = readDoukutsuExe(inFile, args.DECODE, offset=0)
        elif(args.SOURCE.endswith("stage.tbl")):   # CS+
            stageData = readStageTbl(inFile, args.DECODE)
        elif(args.SOURCE.endswith("system.json")): # NXEngine-evo
            stageData = readSystemJSON(inFile)
        elif(args.SOURCE.endswith("mrmap.bin")):   # CSE2
            stageData = readMrmap(inFile, args.DECODE)
        else:                                      # Default, read raw binary
            stageData = readDoukutsuExe(inFile, args.ENCODE, offset=0)
    
    # # Debug
    # for fields in stageData:
    #     print(','.join([ '' if fields[0] is None else fields[0],
    #                      '' if fields[1] is None else fields[1],
    #                      '' if fields[2] is None else ('0x'+fields[2].hex()) if fields[2] else '',
    #                      '' if fields[3] is None else fields[3],
    #                      '' if fields[4] is None else fields[4],
    #                      '' if fields[5] is None else fields[5],
    #                      '' if fields[6] is None else ('0x'+fields[6].hex()) if fields[6] else '',
    #                      '' if fields[7] is None else fields[7],
    #                      '' if len(fields)>7  else '' if fields[8] is None else fields[8]
    #                     ]))

    if(args.DEST == '-'):
        # Assume raw binary if writing to stdout
        outFile = sys.stdout.buffer
        writeDoukutsuExe(outFile, stageData, args.ENCODE, offset=0)
        
    else:
        outFile = open(args.DEST, 'r+b')
    
        if(args.DEST.endswith("Doukutsu.exe")):  # Original
            # Overwrite Doukutsu.exe
            writeDoukutsuExe(outFile, stageData, args.ENCODE)
        elif(args.DEST.endswith(".csmap")):
            writeDoukutsuExe(outFile, stageData, args.ENCODE, offset=0)
        elif(args.DEST.endswith("csmap.bin")):
            writeDoukutsuExe(outFile, stageData, args.ENCODE, offset=0)
        elif(args.DEST.endswith("stage.tbl")):   # CS+
            writeStageTbl(outFile, stageData, args.ENCODE)
        elif(args.DEST.endswith("system.json")): # NXEngine-evo
            writeSystemJSON(outFile, stageData)
        elif(args.DEST.endswith("mrmap.bin")):   # CSE2
            # Overwrite mrmap.bin
            writeMrmap(outFile, stageData, args.ENCODE)
        else:                                    # Default, write raw binary
            writeDoukutsuExe(outFile, stageData, args.ENCODE, offset=0)

    inFile.close()
    outFile.close()
    return

if __name__ == "__main__":
    main()
