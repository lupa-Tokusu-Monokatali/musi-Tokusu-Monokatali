#!/usr/bin/python3
#
# Copyright (C) kulupu pakola li kili nasa
#
# This work is free.  You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To But It's Not My Fault Public
# License, Version 1, as published by Ben McGinnes.  See the
# COPYING.WTFNMFPL text file for more details.

import os
import sys
import argparse
from io import BytesIO
from io import BufferedRWPair

def prettyTSC(inFile, outFile, unpretty=False):
    bPre = b''
    
    
    if(unpretty):
        while(b := inFile.read(1)):
            # outFile.write(b);
            # Remove LFs not preceded by a CR
            if(b == b'\x0a'):
                if(bPre == b'\x0d'):
                    outFile.write(b);
                bPre = b
            else:
                outFile.write(b);
                bPre = b
    else:
        while(b := inFile.read(1)):
            # Add LF before and after commands
            if(b == b'\x3c'):
                if(bPre != b'\x0a'):
                    outFile.write(b'\x0a')
                outFile.write(b);
                cmd = inFile.read(3)
                outFile.write(cmd);
                if(cmd==b'MNP' or cmd==b'SNP' or cmd==b'TRA' ):
                    args = inFile.read(19)
                elif( cmd==b'ANP' or cmd==b'CMP' or cmd==b'CNP' or cmd==b'INP' or cmd==b'TAM' ):
                    args = inFile.read(14)
                elif( cmd==b'AM+' or cmd==b'AMJ' or cmd==b'ECJ' or cmd==b'FLJ' or cmd==b'FOB' or cmd==b'FON' or cmd==b'ITJ' or cmd==b'MOV' or cmd==b'NCJ' or cmd==b'PS+' or cmd==b'SKJ' or cmd==b'SMP' or cmd==b'UNJ' ):
                    args = inFile.read(9)
                elif( cmd==b'AM-' or cmd==b'BOA' or cmd==b'BSL' or cmd==b'CMU' or cmd==b'DNA' or cmd==b'DNP' or cmd==b'EQ+' or cmd==b'EQ-' or cmd==b'EVE' or cmd==b'FAC' or cmd==b'FAI' or cmd==b'FAO' or cmd==b'FL+' or cmd==b'FL-' or cmd==b'FOM' or cmd==b'GIT' or cmd==b'IT+' or cmd==b'IT-' or cmd==b'LI+' or cmd==b'ML+' or cmd==b'MP+' or cmd==b'MPJ' or cmd==b'MYB' or cmd==b'MYD' or cmd==b'NUM' or cmd==b'QUA' or cmd==b'SIL' or cmd==b'SK+' or cmd==b'SK-' or cmd==b'SOU' or cmd==b'SSS' or cmd==b'UNI' or cmd==b'WAI' or cmd==b'XX1' or cmd==b'YNJ' ):
                    args = inFile.read(4)
                elif( cmd==b'AE+' or cmd==b'CAT' or cmd==b'CIL' or cmd==b'CLO' or cmd==b'CLR' or cmd==b'CPS' or cmd==b'CRE' or cmd==b'CSS' or cmd==b'END' or cmd==b'ESC' or cmd==b'FLA' or cmd==b'FMU' or cmd==b'FRE' or cmd==b'HMC' or cmd==b'INI' or cmd==b'KEY' or cmd==b'LDP' or cmd==b'MLP' or cmd==b'MM0' or cmd==b'MNA' or cmd==b'MS2' or cmd==b'MS3' or cmd==b'MSG' or cmd==b'NOD' or cmd==b'PRI' or cmd==b'RMU' or cmd==b'SAT' or cmd==b'SLP' or cmd==b'SMC' or cmd==b'SPS' or cmd==b'STC' or cmd==b'SVP' or cmd==b'TUR' or cmd==b'WAS' or cmd==b'ZAM' ):
                    args = b''
                else:
                    args = b''
                outFile.write(args);
                outFile.write(b'\x0a');
                bPre = b'\x0a'
            else:
                # omit additional LF if there already was one
                if(b == b'\x0a'):
                    if(bPre != b'\x0a'):
                        outFile.write(b);
                else:
                    outFile.write(b);
                bPre = b
                
    return


def main(argv = None):
    parser = argparse.ArgumentParser(
        prog = 'pretty-tsc.py',
        description = '''Add (or remove extra) linefeeds in decoded .tsc scripts
        Remove operation has no effect on unpretty .tsc scripts
        '''
    )
        
    parser.add_argument('-u', '--unpretty', dest='UNPRETTY', action='store_true',
                        help='Reverse operation, removes extra linefeeds')
    parser.add_argument('IN_FILE', nargs='?', default='-')
    parser.add_argument('OUT_FILE', nargs='?', default='-')

    args = parser.parse_args(sys.argv[1:] if argv is None else argv)

    # open files
    if(args.IN_FILE == '-'):
        inFile = sys.stdin.buffer
    else:
        inFile = open(args.IN_FILE, 'rb')
        
    if(args.OUT_FILE == '-'):
        outFile = sys.stdout.buffer
    else:
        outFile = open(args.OUT_FILE, 'wb')
        
    prettyTSC(inFile, outFile, args.UNPRETTY)

    # Hack for special case: Append Pixel's notes to MazeI.txt
    if(args.IN_FILE.endswith("MazeI.txt") and args.UNPRETTY):
        outFile.write("\r\n37: MazeI      0640 - 0659\r\n0640:初到来\r\n0641:扉開く\r\n0642:ライフカプセル\r\n\r\n".encode("utf-8"))
        
    inFile.close()
    outFile.close()
    return

if __name__ == "__main__":
    main()
