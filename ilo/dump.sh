#!/bin/bash

# Find, Convert, and Pretty Print all .tsc files in a directory
find $1 -iname *.tsc -exec sh -c './ilo/tsc-tool.py -d $1 | ./ilo/pretty-tsc.py - ${1%.*}.txt' sh {} \;

# Dump stage data
find $1 -iname 'Doukutsu.exe' -exec sh -c 'xxd -c 0xC8 -s 0x937B0 -l 0x4A38 $1 > ${1%/*}/stage.hex' sh {} \;
