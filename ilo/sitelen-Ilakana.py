#!/usr/bin/python3
#
# Copyright (C) kulupu pakola li kili nasa
#
# This work is free.  You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To But It's Not My Fault Public
# License, Version 1, as published by Ben McGinnes.  See the
# COPYING.WTFNMFPL text file for more details.

import os
import sys
import argparse
from io import BytesIO
from io import BufferedRWPair
import fileinput
import re

SITELEN_ILAKANA = {
     'a' :  'あ',  'an' : 'あん',  'e' :  'え',  'en' :  'えん',  'i' : 'い', 'in'  : 'いん',  'o' : 'お', 'on'  : 'おん',  'u' : 'う',  'un' : 'うん',
    'ka' :  'か', 'kan' : 'かん', 'ke' :  'け', 'ken' :  'けん', 'ki' : 'き', 'kin' : 'きん', 'ko' : 'こ', 'kon' : 'こん', 'ku' : 'く', 'kun' : 'くん',
    'la' :  'ら', 'lan' : 'らん', 'le' :  'れ', 'len' :  'れん', 'li' : 'り', 'lin' : 'りん', 'lo' : 'ろ', 'lon' : 'ろん', 'lu' : 'る', 'lun' : 'るん',
    'ma' :  'ま', 'man' : 'まん', 'me' :  'め', 'men' :  'めん', 'mi' : 'み', 'min' : 'みん', 'mo' : 'も', 'mon' : 'もん', 'mu' : 'む', 'mun' : 'むん',
    'na' :  'な', 'nan' : 'なん', 'ne' :  'ね', 'nen' :  'ねん', 'ni' : 'に', 'nin' : 'にん', 'no' : 'の', 'non' : 'のん', 'nu' : 'ぬ', 'nun' : 'ぬん',
    'pa' :  'ぱ', 'pan' : 'ぱん', 'pe' :  'ぺ', 'pen' :  'ぺん', 'pi' : 'ぴ', 'pin' : 'ぴん', 'po' : 'ぽ', 'pon' : 'ぽん', 'pu' : 'ぷ', 'pun' : 'ぷん',
    'sa' :  'さ', 'san' : 'さん', 'se' :  'せ', 'sen' :  'せん', 'si' : 'し', 'sin' : 'しん', 'so' : 'そ', 'son' : 'そん', 'su' : 'す', 'sun' : 'すん',
    'ta' :  'た', 'tan' : 'たん', 'te' :  'て', 'ten' :  'てん',                             'to' : 'と', 'ton' : 'とん', 'tu' : 'つ', 'tun' : 'つん',
    'wa' :  'わ', 'wan' : 'わん', 'we' :  'ゑ', 'wen' :  'ゑん', 'wi' : 'ゐ', 'win' : 'ゐん',
    'ja' :  'や', 'jan' : 'やん', 'je' : 'いえ', 'jen':'いえん',                             'jo' : 'よ', 'jon' : 'よん', 'ju' : 'ゆ', 'jun' : 'ゆん',
     'n' : 'ん',
     ' a' :  'ああ',  ' an' : 'ああん',  ' e' :  'ええ',  ' en' :  'ええん',  ' i' : 'いい', ' in'  : 'いいん',  ' o' : 'おお', ' on'  : 'おおん',  ' u' : 'うう',  ' un' : 'ううん',
    ' ka' :  'かあ', ' kan' : 'かあん', ' ke' :  'けえ', ' ken' :  'けえん', ' ki' : 'きい', ' kin' : 'きいん', ' ko' : 'こお', ' kon' : 'こおん', ' ku' : 'くう', ' kun' : 'くうん',
    ' la' :  'らあ', ' lan' : 'らあん', ' le' :  'れえ', ' len' :  'れえん', ' li' : 'りい', ' lin' : 'りいん', ' lo' : 'ろお', ' lon' : 'ろおん', ' lu' : 'るう', ' lun' : 'るうん',
    ' ma' :  'まあ', ' man' : 'まあん', ' me' :  'めえ', ' men' :  'めえん', ' mi' : 'みい', ' min' : 'みいん', ' mo' : 'もお', ' mon' : 'もおん', ' mu' : 'むう', ' mun' : 'むうん',
    ' na' :  'なあ', ' nan' : 'なあん', ' ne' :  'ねえ', ' nen' :  'ねえん', ' ni' : 'にい', ' nin' : 'にいん', ' no' : 'のお', ' non' : 'のおん', ' nu' : 'ぬう', ' nun' : 'ぬうん',
    ' pa' :  'ぱあ', ' pan' : 'ぱあん', ' pe' :  'ぺえ', ' pen' :  'ぺえん', ' pi' : 'ぴい', ' pin' : 'ぴいん', ' po' : 'ぽお', ' pon' : 'ぽおん', ' pu' : 'ぷう', ' pun' : 'ぷうん',
    ' sa' :  'さあ', ' san' : 'さあん', ' se' :  'せえ', ' sen' :  'せえん', ' si' : 'しい', ' sin' : 'しいん', ' so' : 'そお', ' son' : 'そおん', ' su' : 'すう', ' sun' : 'すうん',
    ' ta' :  'たあ', ' tan' : 'たあん', ' te' :  'てえ', ' ten' :  'てえん',                                  ' to' : 'とお', ' ton' : 'とおん', ' tu' : 'つう', ' tun' : 'つうん',
    ' wa' :  'わあ', ' wan' : 'わあん', ' we' :  'ゑえ', ' wen' :  'ゑえん', ' wi' : 'ゐい', ' win' : 'ゐいん',
    ' ja' :  'やあ', ' jan' : 'やあん', ' je' : 'いええ', ' jen':'いええん',                                   ' jo' : 'よお', ' jon' : 'よおん', ' ju' : 'ゆう', ' jun' : 'ゆうん'
}

SITELEN_KATAKANA  = {
     'a' :  'ア',  'an' : 'アン',  'e' :  'エ',  'en' :  'エン',  'i' : 'イ', 'in'  : 'イン',  'o' : 'オ', 'on'  : 'オン',  'u' : 'ウ',  'un' : 'ウン',
    'ka' :  'カ', 'kan' : 'カン', 'ke' :  'ケ', 'ken' :  'ケン', 'ki' : 'キ', 'kin' : 'キン', 'ko' : 'コ', 'kon' : 'コン', 'ku' : 'ク', 'kun' : 'クン',
    'la' :  'ラ', 'lan' : 'ラン', 'le' :  'レ', 'len' :  'レン', 'li' : 'リ', 'lin' : 'リン', 'lo' : 'ロ', 'lon' : 'ロン', 'lu' : 'ル', 'lun' : 'ルン',
    'ma' :  'マ', 'man' : 'マン', 'me' :  'メ', 'men' :  'メン', 'mi' : 'ミ', 'min' : 'ミン', 'mo' : 'モ', 'mon' : 'モン', 'mu' : 'ム', 'mun' : 'ムン',
    'na' :  'ナ', 'nan' : 'ナン', 'ne' :  'ネ', 'nen' :  'ネン', 'ni' : 'ニ', 'nin' : 'ニン', 'no' : 'ノ', 'non' : 'ノン', 'nu' : 'ヌ', 'nun' : 'ヌン',
    'pa' :  'パ', 'pan' : 'パン', 'pe' :  'ペ', 'pen' :  'ペン', 'pi' : 'ピ', 'pin' : 'ピン', 'po' : 'ポ', 'pon' : 'ポン', 'pu' : 'プ', 'pun' : 'プン',
    'sa' :  'サ', 'san' : 'サン', 'se' :  'セ', 'sen' :  'セン', 'si' : 'シ', 'sin' : 'シン', 'so' : 'ソ', 'son' : 'ソン', 'su' : 'ス', 'sun' : 'スン',
    'ta' :  'タ', 'tan' : 'タン', 'te' :  'テ', 'ten' :  'テン',                             'to' : 'ト', 'ton' : 'トン', 'tu' : 'ツ', 'tun' : 'ツン',
    'wa' :  'ワ', 'wan' : 'ワン', 'we' :  'ヱ', 'wen' :  'ヱン', 'wi' : 'ヰ', 'win' : 'ヰン',
    'ja' :  'ヤ', 'jan' : 'ヤン', 'je' : 'イエ', 'jen':'イエン',                             'jo' : 'ヨ', 'jon' : 'ヨン', 'ju' : 'ユ', 'jun' : 'ユン',
     'n' : 'ン',
     ' a' :  'アー',  ' an' : 'アアン',  ' e' :  'エー',  ' en' :  'エーン',  ' i' : 'イー', ' in'  : 'イイン',  ' o' : 'オー', ' on'  : 'オーン',  ' u' : 'ウー',  ' un' : 'ウーン',
    ' ka' :  'カー', ' kan' : 'カーン', ' ke' :  'ケー', ' ken' :  'ケーン', ' ki' : 'キー', ' kin' : 'キイン', ' ko' : 'コー', ' kon' : 'コーン', ' ku' : 'クー', ' kun' : 'クーン',
    ' la' :  'ラー', ' lan' : 'ラーン', ' le' :  'レー', ' len' :  'レーン', ' li' : 'リー', ' lin' : 'リイン', ' lo' : 'ロー', ' lon' : 'ローン', ' lu' : 'ルー', ' lun' : 'ルーン',
    ' ma' :  'マー', ' man' : 'マーン', ' me' :  'メー', ' men' :  'メーン', ' mi' : 'ミー', ' min' : 'ミイン', ' mo' : 'モー', ' mon' : 'モーン', ' mu' : 'ムー', ' mun' : 'ムーン',
    ' na' :  'ナー', ' nan' : 'ナーン', ' ne' :  'ネー', ' nen' :  'ネーン', ' ni' : 'ニー', ' nin' : 'ニイン', ' no' : 'ノー', ' non' : 'ノーン', ' nu' : 'ヌー', ' nun' : 'ヌーン',
    ' pa' :  'パー', ' pan' : 'パーン', ' pe' :  'ペー', ' pen' :  'ペーン', ' pi' : 'ピー', ' pin' : 'ピイン', ' po' : 'ポー', ' pon' : 'ポーン', ' pu' : 'プー', ' pun' : 'プーン',
    ' sa' :  'サー', ' san' : 'サーン', ' se' :  'セー', ' sen' :  'セーン', ' si' : 'シー', ' sin' : 'シイン', ' so' : 'ソー', ' son' : 'ソーン', ' su' : 'スー', ' sun' : 'スーン',
    ' ta' :  'ター', ' tan' : 'ターン', ' te' :  'テー', ' ten' :  'テーン',                                  ' to' : 'トー', ' ton' : 'トーン', ' tu' : 'ツー', ' tun' : 'ツーン',
    ' wa' :  'ワー', ' wan' : 'ワーン', ' we' :  'ヱー', ' wen' :  'ヱーン', ' wi' : 'ヰー', ' win' : 'ヰーン',
    ' ja' :  'ヤー', ' jan' : 'ヤーン', ' je' : 'イエー', ' jen':'イエーン',                                   ' jo' : 'ヨー', ' jon' : 'ヨオン', ' ju' : 'ユウ', ' jun' : 'ユウン'
}

# tan https://medium.com/@jayyydyyy/creating-a-regular-expression-to-syllabify-toki-pona-fcf7bff4b3d7
TOKI_PONA_RE  = r"[ptkmnlswj]?(?:(?<=w)[aei]|(?<=[jt])[aeou]|(?<=[pkmnls])[aeiou]|(?<=\b)[aeiou])(?:n(?![nm]?[aeiou]))?|[ ]?[n]"
TOKI_PONA_RE2 = r"[ ]?[ptkmnlswj]?(?:(?<=w)[aei]|(?<=[jt])[aeou]|(?<=[pkmnls])[aeiou]|(?<=\b)[aeiou])(?:n(?![nm]?[aeiou]))?|[ ]?[n]"
TOKI_CAPITAL  = r"[ ]?[AEIOUJKLMNPSTW].*"

def sitelenIlakana(inFile, outFile, LONG_VOWEL=False, SKIP_CMD=False):
    katakana = False
    lineNumber = 0
    for line in inFile:
        lineNumber = lineNumber + 1
        # print(' '.join([str(lineNumber), ': ', str(katakana)]))
        # Ignore script ids and commands
        if line.startswith(b"#"):
            katakana = False
            outFile.write(line)
        elif line.startswith(b"<") or line.startswith(b"\x0d"):
            if not SKIP_CMD:
                outFile.write(line)
        else:
            lineDecoded = line.decode("utf-8")
            if not LONG_VOWEL:
                # # Keep spaces between words
                # for m in re.findall(TOKI_PONA_RE, lineDecoded, re.IGNORECASE):
                #     lineDecoded = lineDecoded.replace(m, SITELEN_ILAKANA[m.lower()], 1)
                m = re.findall(TOKI_PONA_RE2, lineDecoded, re.IGNORECASE)
                for i in range(len(m)):
                    if(i==0):
                        if(m[i].startswith(' ')):
                            if(re.match(TOKI_CAPITAL, m[i])):
                                katakana = True
                                lineDecoded = lineDecoded.replace(m[i], ' ' + SITELEN_KATAKANA[m[i].lower()[1:]], 1)
                            else:
                                katakana = False
                                lineDecoded = lineDecoded.replace(m[i], ' ' + SITELEN_ILAKANA[m[i].lower()[1:]], 1)
                        elif(re.match(TOKI_CAPITAL, m[i])):
                            katakana = True
                            lineDecoded = lineDecoded.replace(m[i], SITELEN_KATAKANA[m[i].lower()], 1) 
                        else:
                            katakana = False
                            lineDecoded = lineDecoded.replace(m[i], SITELEN_ILAKANA[m[i].lower()], 1) 
                    elif(m[i].startswith(' ')):
                        if(re.match(TOKI_CAPITAL, m[i])):
                            katakana = True
                            lineDecoded = lineDecoded.replace(m[i], ' ' + SITELEN_KATAKANA[m[i].lower()[1:]], 1)
                        else:
                            katakana = False
                            lineDecoded = lineDecoded.replace(m[i], ' ' + SITELEN_ILAKANA[m[i].lower()[1:]], 1)
                    else:
                        if(re.match(TOKI_CAPITAL, m[i])):
                            katakana = True
                            lineDecoded = lineDecoded.replace(m[i], SITELEN_KATAKANA[m[i].lower()], 1)
                        else:
                            if(katakana):
                                lineDecoded = lineDecoded.replace(m[i], SITELEN_KATAKANA[m[i].lower()], 1)
                            else:
                                lineDecoded = lineDecoded.replace(m[i], SITELEN_ILAKANA[m[i].lower()], 1)
                
            else:
                # Remove spaces and double each first vowel
                m = re.findall(TOKI_PONA_RE2, lineDecoded, re.IGNORECASE)
                for i in range(len(m)):
                    if(i==0):
                        if(not m[0].startswith(' ')):
                            if(re.match(TOKI_CAPITAL, m[i])):
                                katakana = True
                                lineDecoded = lineDecoded.replace(m[i], SITELEN_KATAKANA[' ' + m[i].lower()], 1)
                            else:
                                katakana = False
                                lineDecoded = lineDecoded.replace(m[i], SITELEN_ILAKANA[' ' + m[i].lower()], 1)
                        else:
                            if(re.match(TOKI_CAPITAL, m[i])):
                                katakana = True
                                lineDecoded = lineDecoded.replace(m[i], SITELEN_KATAKANA[m[i].lower()], 1)
                            else:
                                katakana = False
                                lineDecoded = lineDecoded.replace(m[i], SITELEN_ILAKANA[m[i].lower()], 1)
                    elif(m[i].startswith(' ')):
                        if(re.match(TOKI_CAPITAL, m[i])):
                            katakana = True
                            lineDecoded = lineDecoded.replace(m[i], SITELEN_KATAKANA[m[i].lower()], 1)
                        else:
                            katakana = False
                            lineDecoded = lineDecoded.replace(m[i], SITELEN_ILAKANA[m[i].lower()], 1)
                    else:
                        if(katakana):
                            lineDecoded = lineDecoded.replace(m[i], SITELEN_KATAKANA[m[i].lower()], 1)
                        else:
                            lineDecoded = lineDecoded.replace(m[i], SITELEN_ILAKANA[m[i].lower()], 1)

                                
            outFile.write(lineDecoded.encode("utf-8"))
        
    return

def main(argv = None):
    parser = argparse.ArgumentParser(
        prog = 'sitelen-Ilakana.py',
        description = '''Transliterate sitelen Lasina into sitelen Ilakana
        Requires pretty-tsc format.
        '''
    )
    
    parser.add_argument('-l', '--long-vowel', dest='LONG_VOWEL', action='store_true',
        help='Use long vowels instead of space between words')
    parser.add_argument('-s', '--skip-commands', dest='SKIP_CMD', action='store_true',
        help='Skip commands')
    parser.add_argument('IN_FILE', nargs='?', default='-')
    parser.add_argument('OUT_FILE', nargs='?', default='-')
    
    args = parser.parse_args(sys.argv[1:] if argv is None else argv)

    # open files
    if(args.IN_FILE == '-'):
        inFile = sys.stdin.buffer
    else:
        inFile = open(args.IN_FILE, 'rb')
        
    if(args.OUT_FILE == '-'):
        outFile = sys.stdout.buffer
    else:
        outFile = open(args.OUT_FILE, 'wb')
        
    sitelenIlakana(inFile, outFile, LONG_VOWEL=args.LONG_VOWEL, SKIP_CMD=args.SKIP_CMD)

    # # Special case, append Pixel notes
    # if(args.IN_FILE.endswith("MazeI.txt")):
    #     outFile.write("\r\n37: MazeI      0640 - 0659\r\n0640:初到来\r\n0641:扉開く\r\n0642:ライフカプセル\r\n")
        
    inFile.close()
    outFile.close()
    return
    
if __name__ == "__main__":
    main()
