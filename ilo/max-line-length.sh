#!/bin/bash
find $1 -iname '*.txt' -exec sh -c 'cat $1 | tr -d \r | wc -L $1' sh {} \;
