#!/usr/bin/python3
#
# Copyright (C) kulupu pakola li kili nasa
#
# This work is free.  You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To But It's Not My Fault Public
# License, Version 1, as published by Ben McGinnes.  See the
# COPYING.WTFNMFPL text file for more details.
#
# Adapted from u/dilonshuniikke/
# https://www.reddit.com/r/tokipona/comments/18hs3ia/

import os
import sys
import argparse
from io import BytesIO
from io import BufferedRWPair
import fileinput
import re

from ponadict import codepoints

TOKI_CAPITAL  = r"[ ]?[AEIOUJKLMNPSTW].*"

def sitelenPona(inFile, outFile, SKIP_CMD=False, VERBOSE=False):
    lineNumber = 0
    for line in inFile:
        lineNumber = lineNumber + 1
        
        # Ignore script ids and commands
        if line.startswith(b"#"):
            outFile.write(line)
        elif line.startswith(b"<") or line.startswith(b"\x0d"):
            if not SKIP_CMD:
                outFile.write(line)
        else:
            lineDecoded = line.decode("UTF-8")
            
            # Omit question marks after "seme"
            lineDecoded = re.sub(r'seme[?]+', 'seme.', lineDecoded)
            
            # Get dem wordz with regex
            words = re.findall(r'\w+|\S+?|\r|\n', lineDecoded)
            
            for word in words:

                if(re.match(TOKI_CAPITAL, word)):
                    # Loanwords
                    outFile.write('\U0000E690'.encode("UTF-8"))
                    outFile.write(word.encode("UTF-8"))
                    outFile.write('\U0000E691'.encode("UTF-8"))
                else:
                    # Replace words with their UCSUR codepoints
                    # I tucked all the unicode bullshit away don't
                    # worry about it
                    # Move fron 0xF1900 to 0xE600 PUA, cause NXEngine-evo only support UTF-16
                    if(word in codepoints):
                        # outFile.write(chr(ord(codepoints[word][0])-0xF1900+0xE600).encode("UTF-8"))
                        charCode = ord(codepoints[word][0])
                        outFile.write(chr(charCode - (0xF1900-0xE600 if charCode < 0xFF900 else 0xFF900-0xE000) ).encode("UTF-8"))
                    else:
                        outFile.write(word.encode("UTF-8"))

                    
def main(argv = None):
    parser = argparse.ArgumentParser(
        prog = 'sitelen-Ilakana.py',
        description = '''Transliterate sitelen Lasina into sitelen pona
        Requires pretty-tsc format.
        '''
    )

    parser.add_argument('-s', '--skip-commands', dest='SKIP_CMD', action='store_true',
        help='Skip commands')
    parser.add_argument('-v', '--verbose', dest='VERBOSE', action='store_true',
        help='Skip commands')
    parser.add_argument('IN_FILE', nargs='?', default='-')
    parser.add_argument('OUT_FILE', nargs='?', default='-')
    
    args = parser.parse_args(sys.argv[1:] if argv is None else argv)

    # open files
    if(args.IN_FILE == '-'):
        inFile = sys.stdin.buffer
    else:
        inFile = open(args.IN_FILE, 'rb')
        
    if(args.OUT_FILE == '-'):
        outFile = sys.stdout.buffer
    else:
        outFile = open(args.OUT_FILE, 'wb')
        
    sitelenPona(inFile, outFile, SKIP_CMD=args.SKIP_CMD, VERBOSE=args.VERBOSE)

    # # Special case, append Pixel notes
    # if(args.IN_FILE.endswith("MazeI.txt")):
    #     outFile.write("\r\n37: MazeI      0640 - 0659\r\n0640:初到来\r\n0641:扉開く\r\n0642:ライフカプセル\r\n")

    inFile.close()
    outFile.close()
    return
    
if __name__ == "__main__":
    main()
