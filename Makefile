# o toki ala e ni
MAKEFLAGS += --no-print-directory

# Source game
# MUSI_NANPA_WAN = doukutsu#lang-jp
# MUSI_NANPA_WAN = CaveStory#lang-en
# MUSI_NANPA_WAN_PI_TOKI_NIJON = doukutsu
# MUSI_NANPA_WAN_PI_TOKI_INLI  = CaveStory

# Use `make LANG=` to make all translations
# Use `make LANG=en` for both sitelen-Lasina only
# Use `make LANG=tok` for sitelen-pona only
# Use `make LANG=ja_JP.UTF8` for sitelen-Ilakana (UTF8) only
# Use `make LANG=ja_JP.SJIS` for sitelen-Ilakana-SJIS only
# Use `make LANG=ja` for both sitelen-Ilakana (UTF8) and sitelen-Ilakana-SJIS
# Default behavior is locale dependent
ifneq ($(filter en%, $(LANG)), )
	MUSI_NANPA_WAN = CaveStory
	SITELEN_TOKI = toki-pona
else ifneq ($(filter tok%, $(LANG)), )
	MUSI_NANPA_WAN = CaveStory
	SITELEN_TOKI = sitelen-pona
else ifneq ($(filter %UTF8, $(filter jp%, $(LANG)) ), )
	MUSI_NANPA_WAN = doukutsu
	SITELEN_TOKI = sitelen-Ilakana
else ifneq ($(filter %SJIS, $(filter jp%, $(LANG)) ), )
	MUSI_NANPA_WAN = doukutsu
	SITELEN_TOKI = sitelen-Ilakana-SJIS
else ifneq ($(filter jp%, $(LANG)), )
	MUSI_NANPA_WAN = doukutsu
	SITELEN_TOKI = sitelen-Ilakana sitelen-Ilakana-SJIS
else
	SITELEN_TOKI = toki-pona sitelen-Ilakana sitelen-Ilakana-SJIS sitelen-pona 
endif

ANTE_TARGETS = $(patsubst %,ante/%, $(SITELEN_TOKI))

SITELEN_LASINA       = toki-pona
SITELEN_ILAKANA_UTF8 = sitelen-Ilakana
SITELEN_ILAKANA_SJIS = sitelen-Ilakana-SJIS
SITELEN_PONA         = sitelen-pona

# Platforms
NASIN        ?= nasin/doukutsu-rs nasin/nxengine nasin/nxengine-evo
PALI_TARGETS  = $(patsubst lipu/toki-pona/%.txt, ante/toki-pona/%.tsc, $(NASIN))

SITELEN_LASINA_SOURCES   = $(shell find lipu/toki-pona/ -type f -name '*.txt')
SITELEN_LASINA_TARGETS   = $(patsubst lipu/toki-pona/%.txt, ante/toki-pona/%.tsc, $(SITELEN_LASINA_SOURCES))

SITELEN_ILAKANA_SOURCES  = $(shell find lipu/sitelen-Ilakana/ -type f -name '*.txt');
SITELEN_ILAKANA_TXT      = $(patsubst lipu/toki-pona/%.txt, pali/sitelen-Ilakana/%.txt, $(SITELEN_LASINA_SOURCES))
SITELEN_ILAKANA_UTF8_TSC = $(patsubst pali/sitelen-Ilakana/%.txt, ante/sitelen-Ilakana/%.tsc, $(SITELEN_ILAKANA_TXT))
SITELEN_ILAKANA_SJIS_TSC = $(patsubst pali/sitelen-Ilakana/%.txt, ante/sitelen-Ilakana-SJIS/%.tsc, $(SITELEN_ILAKANA_TXT))

SITELEN_PONA_SOURCES     = $(shell find lipu/sitelen-pona/ -type f -name '*.txt')
SITELEN_PONA_TXT         = $(patsubst lipu/toki-pona/%.txt, pali/sitelen-pona/%.txt, $(SITELEN_LASINA_SOURCES))
SITELEN_PONA_TSC         = $(patsubst pali/sitelen-pona/%.txt, ante/sitelen-pona/%.tsc, $(SITELEN_PONA_TXT))

.PHONY: all       ale ali  \
	clean     weka	   \
	build     pali     \
	test      alasa    \
	ante/sitelen-pona/data/stage.tbl \
	$(NASIN)


.PRECIOUS:
	pakala.txt

all: pali
ale: pali
ali: pali

build: pali
pali: ante #$(NASIN)

# ante: ante/toki-pona ante/sitelen-Ilakana ante/sitelen-Ilakana-SJIS ante/sitelen-pona
ante: $(ANTE_TARGETS)

test: alasa
alasa: pakala.txt

pakala.txt: $(SITELEN_LASINA_SOURCES)
	find lipu/toki-pona/data/Stage/ -name "*.txt" -exec ./ilo/telo-misikeke/public/telo-misikeke {} \; > pakala.txt

$(NASIN):
	@echo $@

# sitelen Lasina
ante/toki-pona:	$(SITELEN_LASINA_TARGETS)		\
		ante/toki-pona/csmap.bin		\
		ante/toki-pona/data/Caret.pbm		\
		ante/toki-pona/data/ItemImage.pbm	\
		ante/toki-pona/data/TextBox.pbm		\
		ante/toki-pona/data/Title.pbm		\
		ante/toki-pona/data/stage.tbl		\
		ante/toki-pona/data/Stage/PrtOside.pbm	\
		ante/toki-pona/data/Npc/NpcSym.pbm	\
		| ante/toki-pona/data/Stage/
ante/toki-pona/:
	mkdir -p ante/toki-pona
ante/toki-pona/csmap.bin:
	rm -f ante/toki-pona/csmap.bin
	touch ante/toki-pona/csmap.bin
	./ilo/cavestage.py lipu/en-dump/Doukutsu.exe ante/toki-pona/csmap.bin
	./ilo/cavestage.py nasin/nxengine-evo/lipu/toki-pona/system.json ante/toki-pona/csmap.bin
ante/toki-pona/data/stage.tbl:
	rm -f ante/toki-pona/data/stage.tbl
	touch ante/toki-pona/data/stage.tbl
	./ilo/cavestage.py lipu/en-dump/Doukutsu.exe ante/toki-pona/data/stage.tbl
	./ilo/cavestage.py nasin/nxengine-evo/lipu/toki-pona/system.json ante/toki-pona/data/stage.tbl
ante/toki-pona/data/:
	mkdir -p ante/toki-pona/data
ante/toki-pona/data/Caret.pbm: | ante/toki-pona/data/
	cp ante/ale/data/Caret.pbm $@
ante/toki-pona/data/ItemImage.pbm: | ante/toki-pona/data/
	cp ante/ale/data/ItemImage.pbm $@
ante/toki-pona/data/Title.pbm: | ante/toki-pona/data/
	cp ante/ale/data/Title_sl.bmp $@
ante/toki-pona/data/TextBox.pbm: | ante/toki-pona/data/
	cp ante/ale/data/TextBox.pbm $@
ante/toki-pona/data/Stage/:
	mkdir -p ante/toki-pona/data/Stage
ante/toki-pona/data/Stage/PrtOside.pbm: | ante/toki-pona/data/Stage/
	cp ante/ale/data/Stage/PrtOside.pbm $@
ante/toki-pona/data/Npc/: 
	mkdir -p ante/toki-pona/data/Npc
ante/toki-pona/data/Npc/NpcSym.pbm: | ante/toki-pona/data/Npc/
	cp ante/ale/data/Npc/NpcSym.pbm $@
ante/toki-pona/data/Stage/:
	mkdir -p ante/toki-pona/data/Stage

# Unpretty and Encode each file
$(SITELEN_LASINA_TARGETS): ante/toki-pona/%.tsc : lipu/toki-pona/%.txt | ante/toki-pona/data/Stage/
	./ilo/pretty-tsc.py -u $< | ./ilo/tsc-tool.py -e - $@

# Patch, Unpretty, and Encode
ante/toki-pona/data/Stage/Almond.tsc: lipu/toki-pona/data/Stage/Almond.txt lipu/toki-pona/data/Stage/Almond.patch | ante/toki-pona/data/Stage/
	patch -o - lipu/toki-pona/data/Stage/Almond.txt lipu/toki-pona/data/Stage/Almond.patch | ./ilo/pretty-tsc.py -u | ./ilo/tsc-tool.py -e - $@
ante/toki-pona/data/Stage/Malco.tsc: lipu/toki-pona/data/Stage/Malco.txt lipu/toki-pona/data/Stage/Malco.patch | ante/toki-pona/data/Stage/
	patch -o - lipu/toki-pona/data/Stage/Malco.txt lipu/toki-pona/data/Stage/Malco.patch | ./ilo/pretty-tsc.py -u | ./ilo/tsc-tool.py -e - $@


# sitelen-Ilakana UTF-8
ante/sitelen-Ilakana:	$(SITELEN_ILAKANA_UTF8_TSC)			\
			ante/sitelen-Ilakana/data/Caret.pbm		\
			ante/sitelen-Ilakana/data/ItemImage.pbm		\
			ante/sitelen-Ilakana/data/TextBox.pbm		\
			ante/sitelen-Ilakana/data/Title.pbm		\
			ante/sitelen-Ilakana/data/stage.tbl		\
			ante/sitelen-Ilakana/data/Stage/PrtOside.pbm	\
			ante/sitelen-Ilakana/data/Npc/NpcSym.pbm	\
			| ante/sitelen-Ilakana/data/Stage/
ante/sitelen-Ilakana: ante/sitelen-Ilakana/data
ante/sitelen-Ilakana/:
	mkdir -p ante/toki-pona
ante/sitelen-Ilakana/data/stage.tbl:
	rm -f ante/sitelen-Ilakana/data/stage.tbl
	touch ante/sitelen-Ilakana/data/stage.tbl
	./ilo/cavestage.py -t utf8 lipu/en-dump/Doukutsu.exe ante/sitelen-Ilakana/data/stage.tbl
	./ilo/cavestage.py -t utf8 nasin/nxengine-evo/lipu/sitelen-Ilakana/system.json ante/sitelen-Ilakana/data/stage.tbl
ante/sitelen-Ilakana/data/:
	mkdir -p ante/sitelen-Ilakana/data
ante/sitelen-Ilakana/data/Caret.pbm: | ante/sitelen-Ilakana/data/
	cp ante/ale/data/Caret.pbm $@
ante/sitelen-Ilakana/data/ItemImage.pbm: | ante/sitelen-Ilakana/data/
	cp ante/ale/data/ItemImage.pbm $@
ante/sitelen-Ilakana/data/Title.pbm: | ante/sitelen-Ilakana/data/
	cp ante/ale/data/Title_jp.bmp $@
ante/sitelen-Ilakana/data/TextBox.pbm: | ante/sitelen-Ilakana/data/
	cp ante/ale/data/TextBox.pbm $@
ante/sitelen-Ilakana/data/Stage/:
	mkdir -p ante/sitelen-Ilakana/data/Stage
ante/sitelen-Ilakana/data/Stage/PrtOside.pbm: | ante/sitelen-Ilakana/data/Stage/
	cp ante/ale/data/Stage/PrtOside.pbm $@
ante/sitelen-Ilakana/data/Npc/: 
	mkdir -p ante/sitelen-Ilakana/data/Npc
ante/sitelen-Ilakana/data/Npc/NpcSym.pbm: | ante/sitelen-Ilakana/data/Npc/
	cp ante/ale/data/Npc/NpcSym.pbm $@
ante/sitelen-Ilakana/data/Stage/:
	mkdir -p ante/sitelen-Ilakana/data/Stage

# Transliterate, Unpretty, and Encode each file
$(SITELEN_ILAKANA_UTF8_TSC): ante/sitelen-Ilakana/%.tsc : lipu/toki-pona/%.txt | ante/sitelen-Ilakana/data/Stage/
	./ilo/sitelen-Ilakana.py $< | ./ilo/pretty-tsc.py -u | ./ilo/tsc-tool.py -e - $@

# Transliterate and Patch
lipu/sitelen-Ilakana/data/Stage/Almond.txt: lipu/toki-pona/data/Stage/Almond.txt lipu/sitelen-Ilakana/data/Stage/Almond.patch
	./ilo/sitelen-Ilakana.py lipu/toki-pona/data/Stage/Almond.txt $@
	patch $@ lipu/sitelen-Ilakana/data/Stage/Almond.patch
lipu/sitelen-Ilakana/data/Stage/Malco.txt: lipu/toki-pona/data/Stage/Malco.txt lipu/sitelen-Ilakana/data/Stage/Malco.patch
	./ilo/sitelen-Ilakana.py lipu/toki-pona/data/Stage/Malco.txt $@
	patch $@ lipu/sitelen-Ilakana/data/Stage/Malco.patch

# Unpretty, and Encode
ante/sitelen-Ilakana/data/Stage/Almond.tsc: lipu/sitelen-Ilakana/data/Stage/Almond.txt lipu/sitelen-Ilakana/data/Stage/Almond.patch | ante/sitelen-Ilakana/data/Stage/
	./ilo/pretty-tsc.py -u lipu/sitelen-Ilakana/data/Stage/Almond.txt | ./ilo/tsc-tool.py -e - $@
ante/sitelen-Ilakana/data/Stage/Malco.tsc: lipu/sitelen-Ilakana/data/Stage/Malco.txt lipu/sitelen-Ilakana/data/Stage/Malco.patch | ante/sitelen-Ilakana/data/Stage/
	./ilo/pretty-tsc.py -u lipu/sitelen-Ilakana/data/Stage/Malco.txt | ./ilo/tsc-tool.py -e - $@


# sitelen-Ilakana Shift-JIS
ante/sitelen-Ilakana:	$(SITELEN_ILAKANA_SJIS_TSC)				\
			ante/sitelen-Ilakana-SJIS/csmap.bin			\
			ante/sitelen-Ilakana-SJIS/data/Caret.pbm		\
			ante/sitelen-Ilakana-SJIS/data/ItemImage.pbm		\
			ante/sitelen-Ilakana-SJIS/data/TextBox.pbm		\
			ante/sitelen-Ilakana-SJIS/data/Title.pbm		\
			ante/sitelen-Ilakana-SJIS/data/stage.tbl		\
			ante/sitelen-Ilakana-SJIS/data/Stage/PrtOside.pbm	\
			ante/sitelen-Ilakana-SJIS/data/Npc/NpcSym.pbm		\
			| ante/sitelen-Ilakana-SJIS/data/Stage/
ante/sitelen-Ilakana-SJIS/:
	mkdir -p ante/sitelen-Ilakana-SJIS
ante/sitelen-Ilakana-SJIS/csmap.bin:
	rm -f ante/sitelen-Ilakana-SJIS/csmap.bin
	touch ante/sitelen-Ilakana-SJIS/csmap.bin
	./ilo/cavestage.py lipu/jp-dump/Doukutsu.exe ante/sitelen-Ilakana-SJIS/csmap.bin
	./ilo/cavestage.py nasin/nxengine-evo/lipu/sitelen-Ilakana/system.json ante/sitelen-Ilakana-SJIS/csmap.bin
ante/sitelen-Ilakana-SJIS/data/stage.tbl:
	rm -f ante/sitelen-Ilakana-SJIS/data/stage.tbl
	touch ante/sitelen-Ilakana-SJIS/data/stage.tbl
	./ilo/cavestage.py -t SJIS lipu/en-dump/Doukutsu.exe ante/sitelen-Ilakana-SJIS/data/stage.tbl
	./ilo/cavestage.py -t SJIS nasin/nxengine-evo/lipu/sitelen-Ilakana/system.json ante/sitelen-Ilakana-SJIS/data/stage.tbl
ante/sitelen-Ilakana-SJIS/data/:
	mkdir -p ante/sitelen-Ilakana-SJIS/data
ante/sitelen-Ilakana-SJIS/data/Caret.pbm: | ante/sitelen-Ilakana-SJIS/data/
	cp ante/ale/data/Caret.pbm $@
ante/sitelen-Ilakana-SJIS/data/ItemImage.pbm: | ante/sitelen-Ilakana-SJIS/data/
	cp ante/ale/data/ItemImage.pbm $@
ante/sitelen-Ilakana-SJIS/data/Title.pbm: | ante/sitelen-Ilakana-SJIS/data/
	cp ante/ale/data/Title_jp.bmp $@
ante/sitelen-Ilakana-SJIS/data/TextBox.pbm: | ante/sitelen-Ilakana-SJIS/data/
	cp ante/ale/data/TextBox.pbm $@
ante/sitelen-Ilakana-SJIS/data/Stage/:
	mkdir -p ante/sitelen-Ilakana-SJIS/data/Stage
ante/sitelen-Ilakana-SJIS/data/Stage/PrtOside.pbm: | ante/sitelen-Ilakana-SJIS/data/Stage/
	cp ante/ale/data/Stage/PrtOside.pbm $@
ante/sitelen-Ilakana-SJIS/data/Npc/: 
	mkdir -p ante/sitelen-Ilakana-SJIS/data/Npc
ante/sitelen-Ilakana-SJIS/data/Npc/NpcSym.pbm: | ante/sitelen-Ilakana-SJIS/data/Npc/
	cp ante/ale/data/Npc/NpcSym.pbm $@
ante/sitelen-Ilakana-SJIS/data/Stage/:
	mkdir -p ante/sitelen-Ilakana-SJIS/data/Stage

# Transliterate, unpretty, and encode each file
$(SITELEN_ILAKANA_SJIS_TSC): ante/sitelen-Ilakana-SJIS/%.tsc : lipu/toki-pona/%.txt | ante/sitelen-Ilakana-SJIS/data/Stage/
	./ilo/sitelen-Ilakana.py $< | ./ilo/pretty-tsc.py -u | iconv -f UTF-8 -t SHIFT-JIS | ./ilo/tsc-tool.py -e - $@

# Patch, Unpretty, and Encode
ante/sitelen-Ilakana-SJIS/data/Stage/Almond.tsc: lipu/toki-pona/data/Stage/Almond.txt lipu/sitelen-Ilakana/data/Stage/Almond.patch | ante/sitelen-Ilakana/data/Stage/
	./ilo/pretty-tsc.py -u lipu/sitelen-Ilakana/data/Stage/Almond.txt | iconv -f UTF-8 -t SHIFT-JIS | ./ilo/tsc-tool.py -e - $@
ante/sitelen-Ilakana-SJIS/data/Stage/Malco.tsc: lipu/toki-pona/data/Stage/Malco.txt lipu/sitelen-Ilakana/data/Stage/Malco.patch | ante/sitelen-Ilakana/data/Stage/
	./ilo/pretty-tsc.py -u lipu/sitelen-Ilakana/data/Stage/Malco.txt | iconv -f UTF-8 -t SHIFT-JIS | ./ilo/tsc-tool.py -e - $@


# sitelen-pona
ante/sitelen-pona:	$(SITELEN_PONA_TSC)				\
			ante/sitelen-pona/data/Caret.pbm		\
			ante/sitelen-pona/data/ItemImage.pbm		\
			ante/sitelen-pona/data/TextBox.pbm		\
			ante/sitelen-pona/data/Title.pbm		\
			ante/sitelen-pona/data/stage.tbl		\
			ante/sitelen-pona/data/Stage/PrtOside.pbm	\
			ante/sitelen-pona/data/Npc/NpcSym.pbm		\
			| ante/sitelen-pona/data/Stage/
ante/sitelen-pona/: ante/sitelen-pona/data
	mkdir -p ante/sitelen-pona
ante/sitelen-pona/data/:
	mkdir -p ante/sitelen-pona/data
ante/sitelen-pona/data/stage.tbl:
	rm -f ante/sitelen-pona/data/stage.tbl
	touch ante/sitelen-pona/data/stage.tbl
	./ilo/cavestage.py -f utf8 -t utf8 lipu/en-dump/Doukutsu.exe ante/sitelen-pona/data/stage.tbl
	./ilo/cavestage.py -t utf8 nasin/nxengine-evo/lipu/sitelen-pona/system.json ante/sitelen-pona/data/stage.tbl
ante/sitelen-pona/data/Caret.pbm: | ante/sitelen-pona/data/
	cp ante/ale/data/Caret.pbm $@
ante/sitelen-pona/data/ItemImage.pbm: | ante/sitelen-pona/data/
	cp ante/ale/data/ItemImage.pbm $@
ante/sitelen-pona/data/Title.pbm: | ante/sitelen-pona/data/
	cp ante/ale/data/Title_sp.bmp $@
ante/sitelen-pona/data/TextBox.pbm: | ante/sitelen-pona/data/
	cp ante/ale/data/TextBox.pbm $@
ante/sitelen-pona/data/Stage/:
	mkdir -p ante/sitelen-pona/data/Stage
ante/sitelen-pona/data/Stage/PrtOside.pbm: | ante/sitelen-pona/data/Stage/
	cp ante/ale/data/Stage/PrtOside.pbm $@
ante/sitelen-pona/data/Npc/: 
	mkdir -p ante/sitelen-pona/data/Npc
ante/sitelen-pona/data/Npc/NpcSym.pbm: | ante/sitelen-pona/data/Npc/
	cp ante/ale/data/Npc/NpcSym.pbm $@
ante/sitelen-pona/data/Stage/:
	mkdir -p ante/sitelen-pona/data/Stage

# Convert, unpretty, and encode each file
$(SITELEN_PONA_TSC): ante/sitelen-pona/%.tsc : lipu/toki-pona/%.txt | ante/sitelen-pona/data/Stage/
	./ilo/sitelen-pona.py $< | ./ilo/pretty-tsc.py -u | ./ilo/tsc-tool.py -e - $@

# Convert and Patch
lipu/sitelen-pona/data/Stage/Almond.txt: lipu/toki-pona/data/Stage/Almond.txt lipu/sitelen-pona/data/Stage/Almond.patch
	./ilo/sitelen-pona.py lipu/toki-pona/data/Stage/Almond.txt $@
	patch $@ lipu/sitelen-pona/data/Stage/Almond.patch
lipu/sitelen-pona/data/Stage/Malco.txt: lipu/toki-pona/data/Stage/Malco.txt lipu/sitelen-pona/data/Stage/Malco.patch
	./ilo/sitelen-pona.py lipu/toki-pona/data/Stage/Malco.txt $@
	patch $@ lipu/sitelen-pona/data/Stage/Malco.patch

# Unpretty, and Encode
ante/sitelen-pona/data/Stage/Almond.tsc: lipu/sitelen-pona/data/Stage/Almond.txt lipu/sitelen-pona/data/Stage/Almond.patch | ante/sitelen-pona/data/Stage/
	./ilo/pretty-tsc.py -u lipu/sitelen-pona/data/Stage/Almond.txt | ./ilo/tsc-tool.py -e - $@
ante/sitelen-pona/data/Stage/Malco.tsc: lipu/sitelen-pona/data/Stage/Malco.txt lipu/sitelen-pona/data/Stage/Malco.patch | ante/sitelen-pona/data/Stage/
	./ilo/pretty-tsc.py -u lipu/sitelen-pona/data/Stage/Malco.txt | ./ilo/tsc-tool.py -e - $@


clean: weka
weka:
	@rm -rf ante/toki-pona/
	@rm -rf ante/sitelen-Ilakana/
	@rm -rf ante/sitelen-Ilakana-SJIS/
	@rm -rf ante/sitelen-pona/


dump: lipu
lipu: lipu/en-dump lipu/jp-dump
lipu/:
	@mkdir -p lipu
lipu/en-dump: nasin/nxengine/CaveStory | lipu/
	@mkdir -p lipu/en-dump
	@rsync -rpE nasin/nxengine/CaveStory/* lipu/en-dump
	@ilo/dump.sh lipu/en-dump
lipu/jp-dump: nasin/nxengine/doukutsu | lipu/
	@mkdir -p lipu/jp-dump
	@rsync -rpE nasin/nxengine/doukutsu/* lipu/jp-dump
	@ilo/dump.sh lipu/jp-dump


# nasin/doukutsu-rs/:
# 	@mkdir -p nasin/doukutsu-rs
# nasin/doukutsu-rs: | nasin/doukutsu-rs/
# 	@cd nasin/doukutsu-rs; make;


nasin/nxengine/:
	@mkdir -p nasin/nxengine
	@cd nasin/nxengine; make

nasin/nxengine-evo/:
	@mkdir -p nasin/nxengine-evo
	@cd nasin/nxengine-evo; make

nasin/doukutsu-rs/:
	@mkdir -p nasin/nxengine-evo
	@cd nasin/nxengine-evo; make



nasin/nxengine-evo/lang-jp: | nasin/nxengine-evo/japanese.zip
	@mkdir -p nasin/nxengine-evo/lang-jp
	@cd nasin/nxengine-evo; unzip japanese.zip -d lang-jp 1> /dev/null
	@ilo/dump.sh nasin/nxengine-evo/lang-jp

nasin/nxengine-evo/japanese.zip:
	@cd nasin/nxengine-evo; wget https://github.com/nxengine/translations/releases/download/v1.12/japanese.zip

nasin/nxengine/doukutsu: | nasin/nxengine/dou_1006.zip
	@cd nasin/nxengine; unzip 'dou_1006.zip' 1> /dev/null

nasin/nxengine/dou_1006.zip: | nasin/nxengine/
	@cd nasin/nxengine; wget 'https://studiopixel.jp/binaries/dou_1006.zip' 1> /dev/null

nasin/nxengine/CaveStory: | nasin/nxengine/cavestoryen.zip
	@cd nasin/nxengine; unzip 'cavestoryen.zip' 1> /dev/null

nasin/nxengine/cavestoryen.zip: | nasin/nxengine/
	@cd nasin/nxengine; wget 'https://www.cavestory.org/downloads/cavestoryen.zip' 1> /dev/null
