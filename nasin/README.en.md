# nasin
[README.md (toki pona)](README.md)

## Freeware / NXEngine

[NXEngine by Caitlin Shaw](https://nxengine.sourceforge.io/)

Supported Platforms:
- Android with Retroarch
- Linux
- Mac OSX (untested)
- Windows (tested with wine on Linux)
- Retroarch

NXEngine only supports sitelen-Lasina

For sitelen-Ilakana on freeware see:

https://forum.winehq.org/viewtopic.php?t=28978



## NXEngine-Evo

[A somewhat upgraded/refactored version of NXEngine by Caitlin Shaw](https://github.com/nxengine/nxengine-evo/releases) 

Supported Platforms:
- Linux
- Mac OSX (untested)
- Windows (untested)
- Vita (untested)
- Switch (untested)

Full support for sitelen-Lasina, sitelen-Ilakana and sitelen-pona.

[NXEngine-evo snap package](https://snapcraft.io/nxengine-evo) 

```
sudo snap install nxengine-evo
```

### NXEngine-Evo Manual Install

TODO

### NXEngine-Evo Setup
<details><summary>Click to expand</summary>
![nxengine-evo-lang-01.png](nxengine-evo/sitelen/nxengine-evo-lang-01.png)

Select "Options"

![nxengine-evo-lang-02.png](nxengine-evo/sitelen/nxengine-evo-lang-02.png)

Navigate to "Language"

![nxengine-evo-lang-03.png](nxengine-evo/sitelen/nxengine-evo-lang-03.png)

Switch to "toki-pona", sitelen Ilakana, or sitelen pona
</details>



## doukutsu-rs

[A fully playable re-implementation of the Cave Story (Doukutsu Monogatari) engine written in Rust.](https://github.com/doukutsu-rs/doukutsu-rs/releases)

Supported Platforms:
- Linux
- Mac OSX (untested)
- Windows (untested)
- Android
- Switch (untested)

Partial support for sitelen-Lasina, sitelen-Ilakana and sitelen-pona.

(Menus and Scripts okay, partial support for images)

### NXEngine-Evo Manual Install

TODO

### doukutsu-rs Setup

Select "Options"


Navigate to "Language"


Switch to "toki-pona", sitelen Ilakana, or sitelen pona.

This will ONLY translate the menus.


Go back to Main Menu

Select "musi ante" (Challenges if in English)

Select "toki-pona", sitelen Ilakana, or sitelen pona.

"sitelen Ilakana SJIS" is for use ONLY with default font ().


## Cave Story+

**EXPERIMENTAL! MAKE BACKUPS BEFORE MODDING**

Supported Platforms:
- Linux
- SteamOS (untested)
- Windows (untested)
- Switch (untested)
