# nasin
[README.en.md (English)](README.en.md)

## nxengine

[ilo NXEngine tan Katalin Sa](https://nxengine.sourceforge.io/)

nasin ni la, sina ken musi:
- ilo Android kepeken Retroarch
- ilo Linux
- ilo Windows (kepeken nasin Wine lon ilo Linux)
- ilo Retroarch

## nxengine-evo

[ilo pona NXEngine tan Katalin Sa](https://github.com/nxengine/nxengine-evo/releases) 

sitelen Ilakana li lon!

nasin ni la, sina ken musi:
- ilo Linux
- ilo Windows (sona lili)
- ilo OSX (sona lili)
- ilo Vita (sona lili)
- ilo Switch (sona lili)

[ilo NXEngine tan nasin Senapu](https://snapcraft.io/nxengine-evo) 

```
sudo snap install nxengine-evo
```

### nasin open

![nxengine-evo-lang-01.png](nxengine-evo/sitelen/nxengine-evo-lang-01.png)

o open e ijo "Options"

![nxengine-evo-lang-02.png](nxengine-evo/sitelen/nxengine-evo-lang-02.png)

o lukin e ijo "Language"

![nxengine-evo-lang-03.png](nxengine-evo/sitelen/nxengine-evo-lang-03.png)

o ante toki "toki-pona".

sina ken ante toki "sitelen-Ilakana" kin.
