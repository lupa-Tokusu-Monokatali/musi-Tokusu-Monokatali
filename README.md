# musi Tokusu Monokatali

mi toki ante e musi Tokusu Monokatali

[README.en.md (English)](README.en.md)

README.jp.md (日本語)

[![Latest Release](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/badges/release.svg)](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/releases)

lipu pi toki ante taso. nasin musi la, o lukin e [nasin](nasin)

# pali

o kama jo e poki ni

```
git clone https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali.git
cd musi-Tokusu-Monokatali
```

## nasin musi

o kama jo e nasin NXEngine, anu nasin NXEngine-Evo, anu nasin Doukutsu-rs.

`git submodule init nasin/nxengine` # nasin Retroarch

`git submodule init nasin/nxengine-evo` # pona

`git submodule init nasin/doukusu-rs` # lukin pona

`git submodule init` # ale

## make 

o make e lipu pi toki ante

`make LANG=` sitelen ale

`make LANG=en` sitelen Lasina taso

`make LANG=jp` sitelen Ilakana taso (UTF8 and SJIS)

`make LANG=tok` sitelen pona taso

## pana en musi

nasin li ante la, sona li ante.  o lukin e ni:

- [nxengine](https://gitlab.com/lupa-Tokusu-Monokatali/nxengine)
- [nxengine-evo](https://gitlab.com/lupa-Tokusu-Monokatali/nxengine-evo)
- [doukusu-rs](https://gitlab.com/lupa-Tokusu-Monokatali/doukusu-rs)

# wile pali
pali mute mute

- [ ] */README.jp.md
- [ ] nasin
  - [x] Freeware lon ilo Windows (kepeken nasin XP)
  - [ ] NXengine
  	- [x] nasin Retroarch
    - [x] ilo Android (kepeken ilo Retroarch)
	- [x] ilo Linux 
    - [ ] ilo Macintosh
  - [ ] NXengine-evo
    - [ ] ilo Macintosh
	- [x] ilo Linux
	- [x] ilo Switch
	- [x] ilo Windows
  - [ ] nasin Doukutsu-rs
    - [x] ilo Android
	- [x] ilo Linux 
    - [ ] ilo Macintosh
	- [ ] ilo Windows
- [x] sitelen toki
  - [x] sitelen Lasina
  - [x] sitelen Ilakana
  - [x] sitelen pona
- [ ] toki ante
  - [x] Doukutsu.exe
  - [ ] mrmap.bin
  - [ ] data
    - [x] data/ArmsItem.tsc
    - [ ] data/Caret.pbm
    - [x] data/Credit.tsc
    - [x] data/Head.tsc
    - [x] data/StageSelect.tsc
    - [ ] data/TextBox.pbm
    - [x] data/Title.pbm
  - [x] data/Npc
    - [x] data/Npc/NpcSym.bmp
  - [x] data/Stage/*
  - [x] data/stage.tbl (doukutsu-rs)
  - [x] data/system.json (NXEngine-evo)
